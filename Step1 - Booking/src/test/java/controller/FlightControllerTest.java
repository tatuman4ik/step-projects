package controller;

import entity.*;
import org.junit.jupiter.api.*;
import java.io.*;
import java.time.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

class FlightControllerTest {
    private FlightController flightController;
    Airline airline;
    City city1;
    City city2;
    LocalDateTime localDateTime;
    private Flight flight;
    private Flight testFlight;
    String expectedOutput;
    List<Flight> resultList;

    @BeforeEach
    void setUp() {
        flightController = new FlightController();
        airline = Airline.RYANAIR;
        city1 = City.AMSTERDAM;
        city2 = City.NEW_YORK_CITY;
        localDateTime = LocalDateTime.of(2023, 5, 10, 8, 30);
        flight = new Flight(airline, city1, city2, localDateTime, 100, 1);
        expectedOutput = "";
        resultList = new ArrayList<>();
    }

    @AfterEach
    void clean() {
        while (flightController.deleteFlightById(1) || flightController.deleteFlightById(2)) {}
    }

    @Test
    void getAllWillGetAllFlightsFromData() {
        List<Flight> flights = flightController.getAllFlights();
        int beginSize = flights.size();

        assertEquals(
                beginSize,
                flightController.countFlights()
        );
        assertFalse(
                flightController.getAllFlights().contains(flight)
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        flights = flightController.getAllFlights();
        int endSize = flights.size();

        assertEquals(
                endSize,
                flightController.countFlights()
        );
        assertEquals(
                beginSize + 1,
                endSize
        );
        assertTrue(
                flightController.getAllFlights().contains(flight)
        );
        assertDoesNotThrow(
                () -> flightController.deleteFlightById(flight.id())
        );
    }

    @Test
    void displayAllFlightsWillPrintEqualExample() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        String expectedOutput;
        expectedOutput = "1.    1   FR     Amsterdam - New York City   10/05/2023  08:30\n";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightController.displayAllFlights()
        );
        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void createNewFlightShouldAddNewFlightToData() {
        assertEquals(
                0,
                flightController.countFlights()
        );

        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        assertEquals(
                1,
                flightController.countFlights()
        );
        assertTrue(
                flightController.getAllFlights().contains(flight)
        );

    }

    @Test
    void deleteByIdWillTrueIfExist() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );
        assertTrue(
                flightController.getAllFlights().contains(flight)
        );

        int flightId = flight.id();
        assertEquals(
                flightController.getFlightById(flightId),
                flight
        );

        int startSize = flightController.countFlights();

        assertTrue(
                flightController.deleteFlightById(flightId)
        );

        assertEquals(
                startSize - 1,
                flightController.getAllFlights().size()
        );
        assertFalse(
                flightController.getAllFlights().contains(flight)
        );
        assertNull(
                flightController.getFlightById(flightId)
        );
    }

    @Test
    void deleteWillFalseIfIdNotExist() {
        assertFalse(
                flightController.getAllFlights().contains(flight)
        );
        int flightId = flight.id();

        assertNull(
                flightController.getFlightById(flightId)
        );

        assertFalse(
                flightController.deleteFlightById(flightId)
        );
    }

    @Test
    void placeReservationWillTrueForVacantPlace() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertTrue(
                flightController.placeReservation(flightId, placeToBook)
        );

        assertFalse(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
    }

    @Test
    void placeReservationWillFalseForReservedPlace() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
        assertTrue(
                flightController.placeReservation(flightId, placeToBook)
        );
        assertFalse(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertFalse(
                flightController.placeReservation(flightId, placeToBook)
        );
    }

    @Test
    void cancellationWillTrueForReservedPlace() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
        assertTrue(
                flightController.placeReservation(flightId, placeToBook)
        );
        assertFalse(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertTrue(
                flightController.cancellation(flightId, placeToBook)
        );

        assertTrue(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
    }

    @Test
    void cancellationWillFalseForVacantPlace() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightController.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertFalse(
                flightController.cancellation(flightId, placeToBook)
        );
    }

    @Test
    void countFlightsWillCountAllFlights() {
        int beginSize = flightController.getAllFlights().size();

        assertEquals(
                beginSize,
                flightController.countFlights()
        );

        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        int endSize = flightController.getAllFlights().size();

        assertEquals(
                endSize,
                flightController.countFlights()
        );

        assertEquals(
                beginSize + 1,
                endSize
        );
    }

    @Test
    void getFlightByIdWillReturnFlightIfExist() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();

        testFlight = flightController.getFlightById(flightId);

        assertEquals(
                flight,
                testFlight
        );
    }

    @Test
    void getFlightByIdWillReturnNullIfNotExist() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();

        assertTrue(
                flightController.deleteFlightById(flightId)
        );

        testFlight = flightController.getFlightById(flightId);

        assertNull(
                testFlight
        );
    }

    @Test
    void vacantCounterWillCountAllVacantPlaces() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();

        int countVacant = 0;
        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            if (place.isVacant()) countVacant++;
        }

        assertEquals(
                countVacant,
                flightController.vacantCounter(flightId)
        );
    }

    @Test
    void vacantCounterWillEqual0WhenAllPlacesReserved() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();

        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            assertTrue(
                    flightController.placeReservation(flightId, place)
            );
        }

        assertEquals(
                0,
                flightController.vacantCounter(flightId)
        );
    }

    @Test
    void showVacantPlacesWillPrintEqualExampleAllPlacesVacant() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();

        expectedOutput = """
                Vacant seats:
                1: \u001B[0;32m1A\u001B[0m \u001B[0;32m1B\u001B[0m \u001B[0;32m1C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m1D\u001B[0m \u001B[0;32m1E\u001B[0m \u001B[0;32m1F\u001B[0m
                2: \u001B[0;32m2A\u001B[0m \u001B[0;32m2B\u001B[0m \u001B[0;32m2C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m2D\u001B[0m \u001B[0;32m2E\u001B[0m \u001B[0;32m2F\u001B[0m
                3: \u001B[0;32m3A\u001B[0m \u001B[0;32m3B\u001B[0m \u001B[0;32m3C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m3D\u001B[0m \u001B[0;32m3E\u001B[0m \u001B[0;32m3F\u001B[0m
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightController.showVacantPlaces(flightId)
        );

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void showVacantPlacesWillPrintEqualExampleAllPlacesReserved() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();

        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            assertTrue(
                    flightController.placeReservation(flightId, place)
            );
        }

        expectedOutput = """
                Vacant seats:
                1: \u001B[0;31m1A\u001B[0m \u001B[0;31m1B\u001B[0m \u001B[0;31m1C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;31m1D\u001B[0m \u001B[0;31m1E\u001B[0m \u001B[0;31m1F\u001B[0m
                2: \u001B[0;31m2A\u001B[0m \u001B[0;31m2B\u001B[0m \u001B[0;31m2C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;31m2D\u001B[0m \u001B[0;31m2E\u001B[0m \u001B[0;31m2F\u001B[0m
                3: \u001B[0;31m3A\u001B[0m \u001B[0;31m3B\u001B[0m \u001B[0;31m3C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;31m3D\u001B[0m \u001B[0;31m3E\u001B[0m \u001B[0;31m3F\u001B[0m
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightController.showVacantPlaces(flightId)
        );

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void showVacantPlacesWillPrintEqualExampleHalfPlacesReserved() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        int flightId = flight.id();

        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            if (place.getSeat().getIndex() % 2 == 0) {
                assertTrue(
                        flightController.placeReservation(flightId, place)
                );
            }
        }

        expectedOutput = """
                Vacant seats:
                1: \u001B[0;31m1A\u001B[0m \u001B[0;32m1B\u001B[0m \u001B[0;31m1C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m1D\u001B[0m \u001B[0;31m1E\u001B[0m \u001B[0;32m1F\u001B[0m
                2: \u001B[0;31m2A\u001B[0m \u001B[0;32m2B\u001B[0m \u001B[0;31m2C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m2D\u001B[0m \u001B[0;31m2E\u001B[0m \u001B[0;32m2F\u001B[0m
                3: \u001B[0;31m3A\u001B[0m \u001B[0;32m3B\u001B[0m \u001B[0;31m3C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m3D\u001B[0m \u001B[0;31m3E\u001B[0m \u001B[0;32m3F\u001B[0m
                """;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightController.showVacantPlaces(flightId)
        );

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void getFlightsFromWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsFrom(city1)
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsFrom(city1)
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void getFlightsToWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsTo(city2)
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsTo(city2)
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void getFlightsFromToWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsFromTo(city1, city2)
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsFromTo(city1, city2)
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void getFlightsFromToDateWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsFromToDate(city1, city2, localDateTime.toLocalDate())
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightController.getFlightsFromToDate(city1, city2, localDateTime.toLocalDate())
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void loadDataWillAddListWith2FlightsToData() {
        assertTrue(
                resultList.add(new Flight(airline, city1, city2, localDateTime, 18, 1))
        );
        assertTrue(
                resultList.add(new Flight(airline, city2, city1, localDateTime, 18, 2))
        );

        assertEquals(
                0,
                flightController.countFlights()
        );

        assertDoesNotThrow(
                () -> flightController.loadData(resultList)
        );

        assertEquals(
                2,
                flightController.countFlights()
        );
    }

    @Test
    void getMaxIdWillBe1With1Flight() {
        assertEquals(
                0,
                flightController.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightController.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightController.countFlights()
        );

        assertEquals(
                1,
                flightController.getMaxId()
        );
    }
}