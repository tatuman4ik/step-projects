package dao;

import entity.*;

import java.time.*;
import java.util.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FlightDaoFileTest {
    private FlightDaoFile flightDaoFile;
    private Flight flight1;
    private Flight flight2;

    @BeforeEach
    void setUp() {
        flightDaoFile = new FlightDaoFile();
        flight1 = new Flight(Airline.RYANAIR, City.AMSTERDAM, City.LONDON, LocalDateTime.now(), 18, -1);
        flight2 = new Flight(Airline.PEGASUS_AIRLINES, City.LONDON, City.BANGKOK, LocalDateTime.now(), 30, -2);
    }

    @AfterEach
    void clean() {
        while (flightDaoFile.delete(-1) || flightDaoFile.delete(-2)) {}
    }

    @Test
    void getAllWillGetAllFlightsFromDaoFile() {
        List<Flight> flights = flightDaoFile.getAll();
        int beginSize = flights.size();

        assertEquals(
                beginSize,
                flightDaoFile.getAll().size()
        );
        assertDoesNotThrow(
                () -> flightDaoFile.save(flight1)
        );

        flights = flightDaoFile.getAll();
        int endSize = flights.size();

        assertEquals(
                endSize,
                flightDaoFile.getAll().size()
        );
        assertEquals(
                beginSize + 1,
                endSize
        );
    }

    @Test
    void getByIdWillGetFlightIdEqualId() {
        assertFalse(
                flightDaoFile.getAll().contains(flight1)
        );
        assertDoesNotThrow(
                () -> flightDaoFile.save(flight1)
        );
        assertTrue(
                flightDaoFile.getAll().contains(flight1)
        );
        assertEquals(
                -1,
                flight1.id()
        );

        Flight testFlight = flightDaoFile.getById(-1);

        assertEquals(
                flight1,
                testFlight
        );
        assertEquals(
                -1,
                testFlight.id()
        );
    }

    @Test
    void getByIdWillReturnNullWhenFlightIdNotExist() {
        assertEquals(
                -1,
                flight1.id()
        );
        assertEquals(
                -2,
                flight2.id()
        );
        assertDoesNotThrow(
                () -> flightDaoFile.save(flight1)
        );
        assertTrue(
                flightDaoFile.getAll().contains(flight1)
        );
        assertFalse(
                flightDaoFile.getAll().contains(flight2)
        );

        Flight testFlight = flightDaoFile.getById(-2);

        assertNull(
                testFlight
        );
        assertFalse(
                flightDaoFile.getAll().contains(testFlight)
        );
    }

    @Test
    void deleteByIdWillTrueIfExist() {
        assertDoesNotThrow(
                () -> flightDaoFile.save(flight1)
        );
        assertEquals(
                -1,
                flight1.id()
        );
        assertTrue(
                flightDaoFile.getAll().contains(flight1)
        );
        assertEquals(
                flightDaoFile.getById(-1),
                flight1
        );

        int startSize = flightDaoFile.getAll().size();

        assertTrue(
                flightDaoFile.delete(-1)
        );
        assertEquals(
                startSize - 1,
                flightDaoFile.getAll().size()
        );
        assertFalse(
                flightDaoFile.getAll().contains(flight1)
        );
        assertNull(
                flightDaoFile.getById(-1)
        );
    }

    @Test
    void deleteWillFalseIfIdNotExist() {
        assertFalse(
                flightDaoFile.getAll().contains(flight1)
        );
        assertEquals(
                -1,
                flight1.id()
        );
        assertNull(
                flightDaoFile.getById(-1)
        );
        assertFalse(
                flightDaoFile.delete(-1)
        );
    }

    @Test
    void saveWillAddToFlightDao() {
        int startSize = flightDaoFile.getAll().size();

        assertFalse(
                flightDaoFile.getAll().contains(flight1)
        );
        assertDoesNotThrow(
                () -> flightDaoFile.save(flight1)
        );
        assertEquals(
                startSize + 1,
                flightDaoFile.getAll().size()
        );
        assertEquals(
                flight1,
                flightDaoFile.getById(-1)
        );
    }

    @Test
    void saveWillAddToFlightDaoWhenChanged() {
        assertFalse(
                flightDaoFile.getAll().contains(flight1)
        );

        assertDoesNotThrow(
                () -> flightDaoFile.save(flight1)
        );
        assertEquals(
                -1,
                flight1.id()
        );
        assertTrue(
                flightDaoFile.getAll().contains(flight1)
        );
        assertEquals(
                flightDaoFile.getById(-1),
                flight1
        );

        Flight testFlight = flight1;
        flight1 = new Flight(Airline.AIR_FRANCE, City.BRUSSELS, City.LONDON, LocalDateTime.now(), 30, -1);

        assertEquals(
                -1,
                flight1.id()
        );
        assertFalse(
                flightDaoFile.getAll().contains(flight1)
        );
        assertNotEquals(
                flightDaoFile.getById(-1),
                flight1
        );

        assertDoesNotThrow(
                () -> flightDaoFile.save(flight1)
        );

        assertTrue(
                flightDaoFile.getAll().contains(flight1)
        );
        assertTrue(
                flightDaoFile.getAll().contains(testFlight)
        );
        assertEquals(
                -1,
                flight1.id()
        );
        assertEquals(
                -1,
                testFlight.id()
        );
        assertEquals(
                2,
                flightDaoFile.getAll().stream().filter(flight -> flight.id() == -1).count()
        );
    }
}