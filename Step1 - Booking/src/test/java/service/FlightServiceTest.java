package service;

import entity.*;
import org.junit.jupiter.api.*;
import java.io.*;
import java.time.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

class FlightServiceTest {
    private FlightService flightService;
    Airline airline;
    City city1;
    City city2;
    LocalDateTime localDateTime;
    private Flight flight;
    private Flight testFlight;
    String expectedOutput;
    List<Flight> resultList;

    @BeforeEach
    void setUp() {
        flightService = new FlightService();
        airline = Airline.RYANAIR;
        city1 = City.AMSTERDAM;
        city2 = City.NEW_YORK_CITY;
        localDateTime = LocalDateTime.of(2023, 5, 10, 8, 30);
        flight = new Flight(airline, city1, city2, localDateTime, 100, 1);
        expectedOutput = "";
        resultList = new ArrayList<>();
    }

    @AfterEach
    void clean() {
        while (flightService.deleteFlightById(1) || flightService.deleteFlightById(2)) {}
    }

    @Test
    void getAllWillGetAllFlightsFromData() {
        List<Flight> flights = flightService.getAllFlights();
        int beginSize = flights.size();

        assertEquals(
                beginSize,
                flightService.countFlights()
        );
        assertFalse(
                flightService.getAllFlights().contains(flight)
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        flights = flightService.getAllFlights();
        int endSize = flights.size();

        assertEquals(
                endSize,
                flightService.countFlights()
        );
        assertEquals(
                beginSize + 1,
                endSize
        );
        assertTrue(
                flightService.getAllFlights().contains(flight)
        );
        assertDoesNotThrow(
                () -> flightService.deleteFlightById(flight.id())
        );
    }

    @Test
    void displayAllFlightsWillPrintEqualExample() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        String expectedOutput;
        expectedOutput = "1.    1   FR     Amsterdam - New York City   10/05/2023  08:30\n";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightService.displayAllFlights()
        );
        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void createNewFlightShouldAddNewFlightToData() {
        assertEquals(
                0,
                flightService.countFlights()
        );

        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        assertEquals(
                1,
                flightService.countFlights()
        );
        assertTrue(
                flightService.getAllFlights().contains(flight)
        );

    }

    @Test
    void deleteByIdWillTrueIfExist() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );
        assertTrue(
                flightService.getAllFlights().contains(flight)
        );

        int flightId = flight.id();
        assertEquals(
                flightService.getFlightById(flightId),
                flight
        );

        int startSize = flightService.countFlights();

        assertTrue(
                flightService.deleteFlightById(flightId)
        );

        assertEquals(
                startSize - 1,
                flightService.getAllFlights().size()
        );
        assertFalse(
                flightService.getAllFlights().contains(flight)
        );
        assertNull(
                flightService.getFlightById(flightId)
        );
    }

    @Test
    void deleteWillFalseIfIdNotExist() {
        assertFalse(
                flightService.getAllFlights().contains(flight)
        );
        int flightId = flight.id();

        assertNull(
                flightService.getFlightById(flightId)
        );

        assertFalse(
                flightService.deleteFlightById(flightId)
        );
    }

    @Test
    void placeReservationWillTrueForVacantPlace() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertTrue(
                flightService.placeReservation(flightId, placeToBook)
        );

        assertFalse(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
    }

    @Test
    void placeReservationWillFalseForReservedPlace() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
        assertTrue(
                flightService.placeReservation(flightId, placeToBook)
        );
        assertFalse(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertFalse(
                flightService.placeReservation(flightId, placeToBook)
        );
    }

    @Test
    void cancellationWillTrueForReservedPlace() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
        assertTrue(
                flightService.placeReservation(flightId, placeToBook)
        );
        assertFalse(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertTrue(
                flightService.cancellation(flightId, placeToBook)
        );

        assertTrue(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );
    }

    @Test
    void cancellationWillFalseForVacantPlace() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();
        Place placeToBook = new Place(1, Seat.A);

        assertTrue(
                flightService.getFlightById(flightId).getBoard().getPlaces().get(1).get(0).isVacant()
        );

        assertFalse(
                flightService.cancellation(flightId, placeToBook)
        );
    }

    @Test
    void countFlightsWillCountAllFlights() {
        int beginSize = flightService.getAllFlights().size();

        assertEquals(
                beginSize,
                flightService.countFlights()
        );

        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );

        int endSize = flightService.getAllFlights().size();

        assertEquals(
                endSize,
                flightService.countFlights()
        );

        assertEquals(
                beginSize + 1,
                endSize
        );
    }

    @Test
    void getFlightByIdWillReturnFlightIfExist() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();

        testFlight = flightService.getFlightById(flightId);

        assertEquals(
                flight,
                testFlight
        );
    }

     @Test
    void getFlightByIdWillReturnNullIfNotExist() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();

         assertTrue(
                 flightService.deleteFlightById(flightId)
         );

        testFlight = flightService.getFlightById(flightId);

         assertNull(
                 testFlight
         );
    }

    @Test
    void vacantCounterWillCountAllVacantPlaces() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();

        int countVacant = 0;
        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            if (place.isVacant()) countVacant++;
        }

        assertEquals(
                countVacant,
                flightService.vacantCounter(flightId)
        );
    }

    @Test
    void vacantCounterWillEqual0WhenAllPlacesReserved() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 100)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();

        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            assertTrue(
                    flightService.placeReservation(flightId, place)
            );
        }

        assertEquals(
                0,
                flightService.vacantCounter(flightId)
        );
    }

    @Test
    void showVacantPlacesWillPrintEqualExampleAllPlacesVacant() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();

        expectedOutput = """
                Vacant seats:
                1: \u001B[0;32m1A\u001B[0m \u001B[0;32m1B\u001B[0m \u001B[0;32m1C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m1D\u001B[0m \u001B[0;32m1E\u001B[0m \u001B[0;32m1F\u001B[0m
                2: \u001B[0;32m2A\u001B[0m \u001B[0;32m2B\u001B[0m \u001B[0;32m2C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m2D\u001B[0m \u001B[0;32m2E\u001B[0m \u001B[0;32m2F\u001B[0m
                3: \u001B[0;32m3A\u001B[0m \u001B[0;32m3B\u001B[0m \u001B[0;32m3C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m3D\u001B[0m \u001B[0;32m3E\u001B[0m \u001B[0;32m3F\u001B[0m
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightService.showVacantPlaces(flightId)
        );

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void showVacantPlacesWillPrintEqualExampleAllPlacesReserved() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();

        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            assertTrue(
                    flightService.placeReservation(flightId, place)
            );
        }

        expectedOutput = """
                Vacant seats:
                1: \u001B[0;31m1A\u001B[0m \u001B[0;31m1B\u001B[0m \u001B[0;31m1C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;31m1D\u001B[0m \u001B[0;31m1E\u001B[0m \u001B[0;31m1F\u001B[0m
                2: \u001B[0;31m2A\u001B[0m \u001B[0;31m2B\u001B[0m \u001B[0;31m2C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;31m2D\u001B[0m \u001B[0;31m2E\u001B[0m \u001B[0;31m2F\u001B[0m
                3: \u001B[0;31m3A\u001B[0m \u001B[0;31m3B\u001B[0m \u001B[0;31m3C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;31m3D\u001B[0m \u001B[0;31m3E\u001B[0m \u001B[0;31m3F\u001B[0m
                """;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightService.showVacantPlaces(flightId)
        );

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void showVacantPlacesWillPrintEqualExampleHalfPlacesReserved() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        int flightId = flight.id();

        for (Place place : flight.getBoard().getPlaces().values().stream().flatMap(List::stream).toList()) {
            if (place.getSeat().getIndex() % 2 == 0) {
                assertTrue(
                        flightService.placeReservation(flightId, place)
                );
            }
        }

        expectedOutput = """
                Vacant seats:
                1: \u001B[0;31m1A\u001B[0m \u001B[0;32m1B\u001B[0m \u001B[0;31m1C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m1D\u001B[0m \u001B[0;31m1E\u001B[0m \u001B[0;32m1F\u001B[0m
                2: \u001B[0;31m2A\u001B[0m \u001B[0;32m2B\u001B[0m \u001B[0;31m2C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m2D\u001B[0m \u001B[0;31m2E\u001B[0m \u001B[0;32m2F\u001B[0m
                3: \u001B[0;31m3A\u001B[0m \u001B[0;32m3B\u001B[0m \u001B[0;31m3C\u001B[0m \u001B[0;34m-aisle- \u001B[0m\u001B[0;32m3D\u001B[0m \u001B[0;31m3E\u001B[0m \u001B[0;32m3F\u001B[0m
                """;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        assertDoesNotThrow(
                () -> flightService.showVacantPlaces(flightId)
        );

        assertEquals(
                expectedOutput,
                outputStream.toString()
        );
    }

    @Test
    void getFlightsFromWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsFrom(city1)
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsFrom(city1)
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void getFlightsToWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsTo(city2)
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsTo(city2)
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void getFlightsFromToWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsFromTo(city1, city2)
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsFromTo(city1, city2)
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void getFlightsFromToDateWillReturnListWith1Flight() {
        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsFromToDate(city1, city2, localDateTime.toLocalDate())
        );
        assertEquals(
                0,
                resultList.size()
        );
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        assertDoesNotThrow(
                () -> resultList = flightService.getFlightsFromToDate(city1, city2, localDateTime.toLocalDate())
        );

        assertEquals(
                1,
                resultList.size()
        );
        assertEquals(
                flight,
                resultList.get(0)
        );
    }

    @Test
    void loadDataWillAddListWith2FlightsToData() {
        assertTrue(
                resultList.add(new Flight(airline, city1, city2, localDateTime, 18, 1))
        );
        assertTrue(
                resultList.add(new Flight(airline, city2, city1, localDateTime, 18, 2))
        );

        assertEquals(
                0,
                flightService.countFlights()
        );

        assertDoesNotThrow(
                () -> flightService.loadData(resultList)
        );

        assertEquals(
                2,
                flightService.countFlights()
        );
    }

    @Test
    void getMaxIdWillBe1With1Flight() {
        assertEquals(
                0,
                flightService.countFlights()
        );
        assertDoesNotThrow(
                () -> flight = flightService.createNewFlight(airline, city1, city2, localDateTime, 18)
        );
        assertEquals(
                1,
                flightService.countFlights()
        );

        assertEquals(
                1,
                flightService.getMaxId()
        );
    }
}