package service;

import constants.Constants;
import entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    private UserService userService;
    private List<User> users;
    @BeforeEach
    void setupThis(){

        User user1 = new User("user1","111", 1);
        User user2 = new User("user2", "111", 2);
        User user3 = new User("user3", "111", 3);
        User user4 = new User("user4", "111", 4);

        userService = new UserService();
        userService.save(user1);
        userService.save(user2);
        userService.save(user3);
        userService.save(user4);

        users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);

    }
    @AfterEach
    void afterThis(){
        userService = null;
        new File(Constants.USERS_FILE_DB).delete();
    }

    @Test
    void testGetAll() {
        assertEquals(users, userService.getAllUsers());
    }

    @Test
    void testGetUserByName() {
        assertEquals(users.get(2),userService.getUserByLogin("user3"));
        assertNull(userService.getUserByLogin("user5"));
    }
}