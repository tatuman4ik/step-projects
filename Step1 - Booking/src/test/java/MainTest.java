import FlightGenerator.Generator;
import entity.Flight;
import libs.Console;
import menu.Menu;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class MainTest {
    @Test
    void test(){
       Generator generator = new Generator();
        List<Flight> data = generator.getFlightData();
        data.forEach(flight -> {
            System.out.println(flight.prettyFormat());
            System.out.println();
        });
        System.out.println(data.get(1).getBoard().showVacantSeats());
        System.out.println(data.get(1).getBoard().showBoardPlacesCount());

    }

}