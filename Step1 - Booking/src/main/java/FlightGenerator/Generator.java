package FlightGenerator;

import controller.FlightController;
import entity.*;

import java.time.*;
import java.util.*;

public class Generator {
    Random random = new Random();
    static final int MAX_FLIGHTS_PER_DAY = 4;
    LocalDate startDate = LocalDate.now();
    LocalDate endDate = startDate.plusWeeks(2);
    FlightController flightController;

    public Generator() {
        flightController = new FlightController();
        List<Flight> flights = new ArrayList<>();
        int maxId = flightController.getMaxId() + 1;
        for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
            for ( int i = 0; i < MAX_FLIGHTS_PER_DAY; i++) {
                Airline airline = Airline.values()[random.nextInt(Airline.values().length)];
                City departure = City.values()[random.nextInt(City.values().length)];
                City destination = Arrays.stream(City.values())
                        .filter(x -> !x.equals(departure))
                        .toList()
                        .get(random.nextInt(City.values().length - 1));
                LocalTime time = LocalTime.of(random.nextInt(24), random.nextInt(12) * 5);
                int capacity = random.nextInt(Board.getMaxCapacity() - Board.getMinCapacity() + 1) + Board.getMinCapacity();
                flights.add(new Flight(airline, departure, destination, date.atTime(time), capacity, maxId++));
            }
        }
        flightController.loadData(flights);
    }

    public List<Flight> getFlightData() {
        return flightController.getAllFlights();
    }
}
