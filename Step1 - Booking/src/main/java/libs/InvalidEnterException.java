package libs;

public class InvalidEnterException extends RuntimeException {
    public InvalidEnterException(String message) {
        super(message);
    }
}
