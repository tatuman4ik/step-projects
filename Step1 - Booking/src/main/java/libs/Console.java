package libs;

/**
 * System.out.print(...) - JUST FORBIDDEN
 * Scanner(System.in) - JUST FORBIDDEN
 */
public class Console {
    public static ScannerConstrained scannerConstrained = new ScannerConstrained();

    public static void println(String line) {
        System.out.println(line);
    }
    public static void print(String line) {
        System.out.print(line);
    }
    public static String read() {
        return scannerConstrained.nextLine();
    }

}
