package controller;

import entity.Booking;
import entity.Ticket;
import entity.User;
import service.BookingService;

import java.util.List;
import java.util.Optional;

public class BookingController {
    BookingService bookingService;

    public BookingController() {
        bookingService = new BookingService();
    }

    public List<Booking> getAllBookings() {
        return bookingService.getAllBookings();
    }

    public Booking createNewBooking(int userId, List<Ticket> tickets) {
        return bookingService.createNewBooking(userId, tickets);
    }

    public List<Booking> getBookingByUser(User user){
        return bookingService.getBookingByUser(user);
    }

    public List<Booking> findBookingByPassenger(String name) {
        return bookingService.findBookingByPassenger(name);
    }

    public Optional<Booking> getBookingByUserAndId(User user, int id){
        return bookingService.getBookingByUserAndId(user, id);
    }
    public boolean delete(int id){
        return bookingService.delete(id);
    }
    public void save(Booking Booking){
        bookingService.save(Booking);
    }

    public int getMaxId(){
        return bookingService.getMaxId();
    }


}
