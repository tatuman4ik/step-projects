package controller;

import entity.User;
import service.UserService;

import java.util.*;

public class UserController {
    private final UserService userService;

    public UserController() {
        userService = new UserService();
    }

    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    public User createNewUser(String login, String password) {
        return userService.createNewUser(login, password);
    }

    public boolean checkUserPassword(String login, String password) {
        return userService.checkUserPassword(login, password);
    }

    public int getMaxId(){
        return userService.getMaxId();
    }

    public User getUserByLogin(String name){
        return userService.getUserByLogin(name);
    }

    public void save(User user){
        userService.save(user);
    }
}
