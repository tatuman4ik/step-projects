package controller;

import entity.*;
import service.FlightService;

import java.time.*;
import java.util.List;

public class FlightController {
    private FlightService flightService;

    public FlightController() { flightService = new FlightService(); }

    public List<Flight> getAllFlights() {
        return flightService.getAllFlights();
    }

    public void displayAllFlights() { flightService.displayAllFlights(); }

    public Flight createNewFlight(Airline airline, City departure, City destination, LocalDateTime localDateTime, int capacity) {
        return flightService.createNewFlight(airline, departure, destination, localDateTime, capacity);
    }

    public boolean deleteFlightById(int id) {
        return flightService.deleteFlightById(id);
    }

    public boolean placeReservation(int flightId, Place place) {
        return flightService.placeReservation(flightId, place);
    }

    public boolean cancellation(int flightIndex, Place place) {
        return flightService.cancellation(flightIndex, place);
    }

    public int countFlights() { return flightService.countFlights(); }

    public Flight getFlightById(int id) { return flightService.getFlightById(id); }

    public int vacantCounter(int index) { return flightService.vacantCounter(index); }

    public void showVacantPlaces(int index) { flightService.showVacantPlaces(index); }

    public List<Flight> getFlightsFrom(City departure) {
        return flightService.getFlightsFrom(departure);
    }

    public List<Flight> getFlightsTo(City destination) {
        return flightService.getFlightsTo(destination);
    }

    public List<Flight> getFlightsFromTo(City departure, City destination) {
        return flightService.getFlightsFromTo(departure, destination);
    }

    public List<Flight> getFlightsFromToDate(City departure, City destination, LocalDate date) {
        return flightService.getFlightsFromToDate(departure, destination, date);
    }

    public void loadData(List<Flight> flights){
        flightService.loadData(flights);
    }
    public int getMaxId(){
        return flightService.getMaxId();
    }
}
