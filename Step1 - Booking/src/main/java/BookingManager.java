import FlightGenerator.Generator;
import controller.*;
import entity.*;
import libs.*;
import menu.*;

import java.time.*;
import java.time.format.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BookingManager {
    private boolean isRunning = true;
    private User user = new User();
    private Menu menu = new Menu();
    private final UserController userController = new UserController();
    private final FlightController flightController = new FlightController();
    private final BookingController bookingController = new BookingController();

    public void run(){
        while (isRunning) {
            try {
                if (user.getLogin().equals("guest")) {
                    menu.menuForGuest();
                } else {
                    menu.menuForRegisterUser();
                }
                menu.printMenu();
                String userChoice = enterText("Enter menu number >>>> ");
                if (userChoice.equalsIgnoreCase("exit")) break;
                runMenu(menu.getMenuCommandsByIndex(Integer.parseInt(userChoice)));
            } catch (InvalidEnterException e) {
                Console.println(ConsoleColor.RED + "Invalid menu choice. Please select a valid option" + ConsoleColor.RESET);
            }
        }
    }

    private void runMenu(MenuCommands menuCommandsByIndex) {
        switch (menuCommandsByIndex){
            case LOGIN -> login();
            case LOGOUT  -> logout();
            case REGISTER -> registerUser();
            case VIEW_TIMETABLE -> viewTimetable();
            case VIEW_FLIGHT -> viewFlight();
            case SEARCH_AND_BOOK_FLIGHT -> searchAndBookFlight();
            case VIEW_BOOKING -> viewBooking();
            case CANCEL_BOOKING -> cancelBooking();
            case FIND_BY_PASSENGER -> findByUserAndPassenger();
            case EXIT -> exit();
        }
    }

    private void login(){
        while (user.getLogin().equals("guest")) {
            String userLogin = enterText("Enter your login: >>>> ");
            if (userLogin.equalsIgnoreCase("EXIT")) { break; }
            String userPassword = enterText("Enter your password: >>>> ");
            if (userPassword.equalsIgnoreCase("EXIT")) { break; }
            if (userController.checkUserPassword(userLogin, userPassword)) {
                user = userController.getUserByLogin(userLogin);
            } else Console.println("This name or password is not correct. Try again.");
        }
    }

    private void logout() {
        user = new User();
    }

    private void registerUser() {
        outerLoop:
        while (user.getLogin().equals("guest")) {
            String matcherName = "[a-zA-Z]{2,20}";
            //String matcherPassword = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
            String userLogin;
            String userPassword;
            while (true) {
                userLogin = enterText("Enter your name: >>>> ");
                if (userLogin.equalsIgnoreCase("EXIT")) break outerLoop;
                if (userLogin.equalsIgnoreCase("guest")
                        || userController.getUserByLogin(userLogin) != null) {
                    Console.println("This name is already in use.");
                } else break;
            }
            while (true) {
                userPassword = enterText("Enter your password: >>>> ");
                //String userPassword = enterTextWithMatcher("Enter your password: >>>> ", matcherPassword);
                if (userPassword.equalsIgnoreCase("EXIT")) { break outerLoop; }
                String confUserPassword = enterText("Enter password confirmation: >>>> ");
                //String confUserPassword = enterTextWithMatcher("Enter password confirmation: >>>> ", matcherPassword);
                if (userPassword.equalsIgnoreCase("EXIT")) { break outerLoop; }
                if (!userPassword.equals(confUserPassword)) {
                    Console.println("You password is not equals password confirmation. Try again");
                } else break;
            }
            user = userController.createNewUser(userLogin, userPassword);
        }
    }

    private void viewTimetable() {
        List<Flight> allFlights = flightController.getAllFlights();
        if (allFlights.size()==0){
            new Generator();
            Console.println("Generate...");
        }
        flightController.getAllFlights()
                .stream()
                .filter(
                        flight -> flight.getFlightDateTime().isAfter(LocalDateTime.now())
                                && flight.getFlightDateTime().isBefore(LocalDateTime.now().plusHours(24))
                )
                .sorted(Comparator.comparing(Flight::getFlightDateTime))
                .forEach(flight -> Console.println(flight.prettyFormat() + "\n"));
    }

    private void viewFlight() {
        int flightId = 0;
        while (flightId == 0 && flightController.getFlightById(flightId) == null) {
            try {
                String idString = enterText("Enter id flight: >>>> ");
                if (idString.equalsIgnoreCase("exit")
                        || idString.equalsIgnoreCase("0")) return;
                flightId = Integer.parseInt(idString);
            } catch (InvalidEnterException e) {
                Console.println("No such index. Try again");
            }
        }
        Flight flightById = flightController.getFlightById(flightId);
        Console.println(flightById.prettyFormat());
        Console.println(flightById.getBoard().showVacantSeats());
        Console.println(flightById.getBoard().showBoardPlacesCount());
    }

    private void searchAndBookFlight() {
        City departure = null;
        while (departure == null) {
            String cityName = enterText("Enter departure: >>>> ");
            if (cityName.equalsIgnoreCase("EXIT")) return;
            departure = Arrays.stream(City.values())
                    .filter(city ->
                            city.getName().equalsIgnoreCase(cityName))
                    .findAny()
                    .orElse(null);
        }
        City destination = null;
        while (destination == null) {
            String cityName = enterText("Enter destination: >>>> ");
            if (cityName.equalsIgnoreCase("EXIT")) return;
            destination = Arrays.stream(City.values())
                    .filter(city ->
                            city.getName().equalsIgnoreCase(cityName))
                    .findAny()
                    .orElse(null);
        }
        LocalDate date = null;
        while (date == null) {
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                String dateString = enterText("Enter date (ex. 01.05.2023) : >>>> ");
                if (dateString.equalsIgnoreCase("exit")) return;
                date = LocalDate.parse(dateString, formatter);
            } catch (InvalidEnterException | DateTimeException e) {
                Console.println(ConsoleColor.RED + "Incorrect data format" + ConsoleColor.RESET);
            }
        }
        int numTickets = 0;
        while (numTickets == 0) {
            try {
                String numString = enterText("Enter number of passengers : >>>> ");
                if (numString.equalsIgnoreCase("exit")) return;
                numTickets = Integer.parseInt(numString);
            } catch (InvalidEnterException e) {
                Console.println(ConsoleColor.RED + "This number is not correct. Try again." + ConsoleColor.RESET);
            }
        }
        final int numPassengers = numTickets;
        List<Flight> flights = flightController
                .getFlightsFromToDate(departure, destination, date)
                .stream()
                .filter(flight ->
                        flightController.vacantCounter(flight.id()) >= numPassengers)
                .toList();
        if (flights.size() == 0) {
            Console.println(ConsoleColor.RED + "No flights for this parameters. Try to change it" + ConsoleColor.RESET);
        } else {
            IntStream.range(0, flights.size())
                    .mapToObj(i -> (i + 1) + ". " + flights.get(i).prettyFormat())
                    .forEach(Console::println);
            int flightId = 0;
            while (flightId == 0 && !flights.contains(flightController.getFlightById(flightId))) {
                try {
                    String idString = enterText("Enter id flight: >>>> ");
                    if (idString.equalsIgnoreCase("exit")
                            || idString.equalsIgnoreCase("0")) return;
                    flightId = Integer.parseInt(idString);
                } catch (InvalidEnterException e) {
                    Console.println(ConsoleColor.RED + "No such index. Try again" + ConsoleColor.RESET);
                }
            }
            Flight flightForUser = flightController.getFlightById(flightId);
            int count = 0;
            List<Ticket> tickets = new ArrayList<>();
            while (count < numPassengers) {
                try {
                    Console.println(flightForUser.getBoard().showVacantSeats());
                    String passenger = enterText("Enter passenger name (ex. Vasiliy Rogov) : >>>> ");
                    if (passenger.equalsIgnoreCase("EXIT")) return;
                    String placeString = enterText("Enter place (ex. 4D): >>>> ");
                    if (placeString.equalsIgnoreCase("EXIT")) return;
                    int row = Integer.parseInt(placeString.substring(0, placeString.length() - 1));
                    Seat seat =  Seat.valueOf(placeString.substring(placeString.length() - 1));
                    Place place = new Place(row, seat);
                    if (flightController.placeReservation(flightId, place)) {
                        tickets.add(new Ticket(passenger, flightForUser, place));
                        count++;
                    } else
                        Console.println(ConsoleColor.MAGENTA_BRIGHT + "This place is not vacant. Try other" + ConsoleColor.RESET);
                } catch (InvalidEnterException e) {
                    Console.println(ConsoleColor.RED + "Try to enter carefully" + ConsoleColor.RESET);
                }
            }
            bookingController.createNewBooking(user.id(), tickets);
            Console.println(ConsoleColor.BLUE + "Success!" + ConsoleColor.RESET);
        }
    }

    private void viewBooking() {
        if (!user.getLogin().equals("guest")){
            String bookingToString = bookingController.getBookingByUser(user)
                    .stream()
                    .map(Booking::prettyFormat)
                    .collect(Collectors.joining("\n"));
            Console.println(bookingToString);
        }
    }

    private void cancelBooking() {
        boolean marker = false;
        while (!marker) {
            try {
                viewBooking();
                String idBooking = enterText("Enter id booking: >>>> ");
                if (idBooking.equalsIgnoreCase("exit")) break;
                int id = Integer.parseInt(idBooking);
                Optional<Booking> bookingById = bookingController.getBookingByUserAndId(user, id);
                bookingById.ifPresent(booking -> booking
                        .tickets()
                        .forEach(ticket -> flightController.cancellation(ticket.getFlight().id(), ticket.getPlace())));
                marker = bookingController.delete(id);
            } catch (InvalidEnterException e) {
                Console.println(ConsoleColor.RED + "No such id" + ConsoleColor.RESET);
            }
        }
    }

    private void findByUserAndPassenger() {
        String name = enterText("Enter your name (ex. Vasiliy Rogov) : >>>> ");
        if (!name.equalsIgnoreCase("EXIT")) {
            viewBooking();
            Console.println(bookingController
                    .findBookingByPassenger(name)
                    .stream()
                    .map(Booking::prettyFormat)
                    .collect(Collectors.joining("\n")));
        }
    }

    private void exit() {
        this.isRunning = false;
    }

    private String enterText(String message){
        Console.print(ConsoleColor.GREEN_UNDERLINED + message + ConsoleColor.RESET);
        return Console.read();
    }
}
