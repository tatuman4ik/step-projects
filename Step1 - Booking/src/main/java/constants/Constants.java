package constants;

public class Constants {
    public static final String USERS_FILE_DB = "users.dat";
    public static final String BOOKINGS_FILE_DB = "bookings.dat";
}
