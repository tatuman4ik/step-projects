package menu;

import libs.Console;
import libs.ConsoleColor;

import java.util.List;

import static menu.MenuCommands.*;

public class Menu {
    private List<MenuCommands> menuCommands;

    public void menuForRegisterUser(){
        menuCommands = List.of(
                VIEW_TIMETABLE,
                VIEW_FLIGHT,
                SEARCH_AND_BOOK_FLIGHT,
                VIEW_BOOKING,
                CANCEL_BOOKING,
                FIND_BY_PASSENGER,
                LOGOUT);
    }

    public void menuForGuest(){
        menuCommands = List.of(
                LOGIN,
                REGISTER,
                VIEW_TIMETABLE,
                VIEW_FLIGHT,
                FIND_BY_PASSENGER,
                EXIT);
    }

    public void printMenu(){
        Console.println(lineToString());
        Console.println(headerToString());
        Console.println(lineToString());

        for (int i = 0; i < menuCommands.size(); i++) {
            Console.println(menuItemToString(i, menuCommands.get(i)));
        }
        Console.println(lineToString());
    }

    private String lineToString(){
        return "=".repeat(60);
    }
    private String headerToString(){
        return "\t".repeat(5)+ConsoleColor.CYAN+ "FLIGHT BOOKING MANAGER"+ConsoleColor.RESET;
    }
    private String menuItemToString(int i, MenuCommands item){
        return "\t".repeat(5)+String.format("%d. %s",++i,item.getDescription());
    }

    public boolean isValidIndex(String userChoice) {
        return userChoice.matches("^[1-9]$")
                && Integer.parseInt(userChoice) >= 0
                && Integer.parseInt(userChoice) <= menuCommands.size();
    }

    public MenuCommands getMenuCommandsByIndex(int index){
        return menuCommands.get(index-1);
    }

}
