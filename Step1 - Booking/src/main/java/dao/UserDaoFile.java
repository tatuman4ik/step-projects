package dao;


import entity.User;
import libs.EX;

import java.io.*;
import java.util.*;
public class UserDaoFile implements DAO<User>{

    private final File file;
    private List<User> users;

    public UserDaoFile(File file) {
        this.file = file;
        if (file.exists()){
            this.users = this.getAll();
        }else {
            this.users = new ArrayList<>();
        }
    }

    @Override
    public List<User> getAll() {

        if (!file.exists()) return this.users;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){

            this.users = (List<User>) ois.readObject();
           
        } catch (FileNotFoundException e) {
          //  throw EX.NI; //запись в логирование
            throw new RuntimeException(e);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return this.users;
    }

    @Override
    public User getById(int id) {

        return this.users
                .stream()
                .filter(user -> user.id() == id)
                .findAny()
                .orElse(null);
    }

    @Override
    public boolean delete(int id) {

        if (this.users.isEmpty()) return false;

        this.users = this.users
                .stream()
                .filter(user -> user.id() != id).toList();

        loadData(this.users);

        return true;
    }

    @Override
    public void save(User user) {

        if (!this.users.isEmpty() && this.users.contains(user)) {
            int index = this.users.indexOf(user);
            this.users.set(index, user);
        } else{
            this.users.add(user);
        }

        loadData(this.users);
    }
    @Override
    public void loadData(List<User> allUser) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(allUser);
        } catch (IOException e) {
            throw EX.EWRITEF;
        }
    }
}
