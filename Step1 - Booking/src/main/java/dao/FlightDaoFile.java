package dao;

import entity.Flight;
import libs.EX;

import java.io.*;
import java.util.*;

public class FlightDaoFile implements DAO<Flight> {
    String filePath = System.getProperty("user.dir") + File.separator + "flights.dat";
    private final File file = new File(filePath);
    private List<Flight> flights = new ArrayList<>();

    public FlightDaoFile() {
        try {
            if (file.createNewFile()) {
                loadData(flights);
            } else getAll();
        } catch (IOException e) {
            throw EX.ECREATF;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Flight> getAll() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            flights = (List<Flight>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw EX.EREADF;
        }
        return flights;
    }

    @Override
    public Flight getById(int id) {
        try {
            return flights.stream().filter(flight -> flight.id() == id).findAny().orElse(null);
        } catch (IndexOutOfBoundsException e) {
            throw EX.EGETIND;
        }
    }

    @Override
    public boolean delete(int id) {
        Flight flightToRemove = getById(id);
        boolean isDeleted = flights.remove(flightToRemove);
        loadData(flights);
        return isDeleted;
    }

    @Override
    public void save(Flight flight) {
        if (!flights.isEmpty() && flights.contains(flight)) {
            int index = flights.indexOf(flight);
            flights.set(index, flight);
        } else
            flights.add(flight);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))){
            oos.writeObject(flights);
        } catch (IOException e) {
            throw EX.EWRITEF;
        }
    }

    @Override
    public void loadData(List<Flight> flightsToLoad) {
        for (Flight newFlight : flightsToLoad) {
            if (!flights.isEmpty() && flights.contains(newFlight)) {
                int index = flights.indexOf(newFlight);
                flights.set(index, newFlight);
            } else
                flights.add(newFlight);
        }
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))){
            oos.writeObject(flights);
        } catch (IOException e) {
            throw EX.EWRITEF;
        }
    }
}
