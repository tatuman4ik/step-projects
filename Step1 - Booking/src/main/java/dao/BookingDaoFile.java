package dao;

import entity.Booking;
import libs.EX;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BookingDaoFile implements DAO<Booking>{
    private final File file;
    private List<Booking> bookings;

    public BookingDaoFile(File file) {
        this.file = file;
        this.bookings = new ArrayList<>();

    }

    @Override
    public List<Booking> getAll() {
        if (!file.exists()) return this.bookings;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){

            this.bookings = (List<Booking>) ois.readObject();

        } catch (FileNotFoundException e) {
            //  throw EX.NI; //запись в логирование
            throw new RuntimeException(e);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return this.bookings;
    }

    @Override
    public Booking getById(int id) {
        return this.bookings
                .stream()
                .filter(user -> user.id() == id)
                .findAny()
                .orElse(null);
    }

    @Override
    public boolean delete(int id) {
        if (this.bookings.isEmpty()) return false;

        this.bookings = this.bookings
                .stream()
                .filter(user -> user.id() != id).toList();

        loadData(this.bookings);

        return true;
    }

    @Override
    public void save(Booking booking) {
        if (!this.bookings.isEmpty() && this.bookings.contains(booking)) {
            int index = this.bookings.indexOf(booking);
            this.bookings.set(index, booking);
        } else{
            this.bookings.add(booking);
        }

        loadData(this.bookings);

    }
    @Override
    public void loadData(List<Booking> allBooking) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(allBooking);
        } catch (IOException e) {
            throw EX.EWRITEF;
        }
    }

}
