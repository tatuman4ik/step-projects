package dao;

import libs.EX;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DaoFile<A  extends Identifiable> implements DAO<A>{
    private final File file;
    private List<A> list;

    public DaoFile(File file) {
        this.file = file;

        if (file.exists()){
            list = this.getAll();
        }else {
            list = new ArrayList<>();
        }

    }

    @Override
    public List<A> getAll() {
        if (!file.exists()) return this.list;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))){

            this.list = (List<A>) ois.readObject();

        } catch (FileNotFoundException e) {
            //  throw EX.NI; //запись в логирование
            throw new RuntimeException(e);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        return this.list;
    }

    @Override
    public A getById(int id) {
        return this.list
                .stream()
                .filter(user -> user.id() == id)
                .findAny()
                .orElse(null);
    }

    @Override
    public boolean delete(int id) {
        if (this.list.isEmpty()) return false;

        this.list = this.list
                .stream()
                .filter(user -> user.id() != id).toList();

        loadData(this.list);

        return true;
    }

    @Override
    public void save(A a) {

            this.list.add(a);

        loadData(this.list);

    }
    public void loadData(List<A> list) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(list);
        } catch (IOException e) {
            throw EX.EWRITEF;
        }
    }

    public int getMaxId(){

        return this.getAll()
                .stream()
                .mapToInt(A::id)
                .max()
                .orElse(-1);

    }
}
