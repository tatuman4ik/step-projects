package service;

import constants.Constants;
import dao.*;
import entity.*;

import java.io.File;
import java.util.*;

public class UserService {
    private final DaoFile<User> userDaoFile;

    public UserService() {
        userDaoFile = new DaoFile<>(new File(Constants.USERS_FILE_DB));
    }

    public List<User> getAllUsers(){
        return userDaoFile.getAll();
    }

    public User createNewUser(String login, String password) {
        int id = getAllUsers().size();
        if (id > 0) id = getMaxId();
        User newUser = new User(login, password, ++id);
        userDaoFile.save(newUser);
        return newUser;
    }

    public boolean checkUserPassword(String login, String password) {
        return getUserByLogin(login) != null && getUserByLogin(login).getPassword().equals(password);
    }

    public void save(User user){
        userDaoFile.save(user);
    }

    public User getUserByLogin(String login){
        return userDaoFile
                .getAll()
                .stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    public int getMaxId(){
        return userDaoFile.getAll()
                .stream()
                .mapToInt(User::id)
                .max()
                .orElse(0);
    }
}
