package service;

import dao.*;
import entity.*;
import libs.*;

import java.time.*;
import java.util.*;
import java.util.stream.*;

public class FlightService {
    private DAO<Flight> flightDao;

    public FlightService() {
        flightDao = new FlightDaoFile();
    }

    public List<Flight> getAllFlights() {
        return flightDao.getAll();
    }

    public void displayAllFlights() {
        List<Flight> flights = flightDao.getAll();
        IntStream.range(0, flights.size())
                .mapToObj(i -> (i + 1) + ". " + flights.get(i).prettyFormat())
                .forEach(Console::println);
    }

    public Flight createNewFlight(Airline airline, City departure, City destination, LocalDateTime localDateTime, int capacity) {
        int id = countFlights();
        if (id > 0) {
            id = getMaxId();
        }
        Flight newFlight = new Flight(airline, departure, destination, localDateTime, capacity, ++id);
        flightDao.save(newFlight);
        return newFlight;
    }

    public boolean deleteFlightById(int id) {
        return flightDao.delete(id);
    }

    public boolean placeReservation(int flightId, Place place) {
        Flight flight = flightDao.getById(flightId);
        boolean isReserved = flight.getBoard().bookPlace(place.getRow(), place.getSeat());
        flightDao.save(flight);
        return isReserved;
    }

    public boolean cancellation(int flightId, Place place) {
        Flight flight = flightDao.getById(flightId);
        boolean cancelled = flight.getBoard().cancellation(place.getRow(), place.getSeat());
        flightDao.save(flight);
        return cancelled;
    }

    public int countFlights() { return flightDao.getAll().size(); }

    public Flight getFlightById(int id) { return flightDao.getById(id); }

    public int vacantCounter(int id) { return flightDao.getById(id).getBoard().countVacantSeats(); }

    public void showVacantPlaces(int id) { Console.println(flightDao.getById(id).getBoard().showVacantSeats()); }

    public List<Flight> getFlightsFrom(City departure) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDeparture().equals(departure))
                .toList();
    }

    public List<Flight> getFlightsTo(City destination) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDestination().equals(destination))
                .toList();
    }

    public List<Flight> getFlightsFromTo(City departure, City destination) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDeparture().equals(departure))
                .filter(flight -> flight.getDestination().equals(destination))
                .toList();
    }

    public List<Flight> getFlightsFromToDate(City departure, City destination, LocalDate date) {
        return flightDao.getAll()
                .stream()
                .filter(flight -> flight.getDeparture().equals(departure))
                .filter(flight -> flight.getDestination().equals(destination))
                .filter(flight -> flight.getFlightDateTime().toLocalDate().equals(date)
                                && flight.getFlightDateTime().isAfter(LocalDateTime.now()))
                .toList();
    }

    public void loadData(List<Flight> flights){
        flightDao.loadData(flights);
    }

    public int getMaxId(){
        return flightDao.getAll()
                .stream()
                .mapToInt(Flight::id)
                .max()
                .orElse(0);
    }
}
