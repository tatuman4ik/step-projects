package service;

import constants.Constants;
import dao.DaoFile;
import entity.*;

import java.io.File;
import java.util.*;

public class BookingService {
    private DaoFile<Booking> bookingDaoFile;

    public BookingService() {
        bookingDaoFile = new DaoFile<>(new File(Constants.BOOKINGS_FILE_DB));
    }

    public List<Booking> getAllBookings(){
        return bookingDaoFile.getAll();
    }

    public Booking createNewBooking(int userId, List<Ticket> tickets) {
        int id = getAllBookings().size();
        if (id > 0) id = getMaxId();
        Booking newBooking = new Booking(userId, tickets, ++id);
        bookingDaoFile.save(newBooking);
        return newBooking;
    }

    public List<Booking> getBookingByUser(User user){
        return getAllBookings()
                .stream()
                .filter(booking -> booking.userId() == user.id())
                .toList();
    }

    public List<Booking> findBookingByPassenger(String name) {
        return getAllBookings()
                .stream()
                .filter(
                        booking -> booking.tickets()
                                .stream()
                                .anyMatch(ticket -> ticket.getPassenger().equalsIgnoreCase(name))
                )
                .toList();
    }

    public Optional<Booking> getBookingByUserAndId(User user, int id){
        return this.getAllBookings()
                .stream()
                .filter(booking -> booking.userId() == user.id()
                        && booking.id() == id)
                .findAny();
    }

    public void save(Booking booking){
        bookingDaoFile.save(booking);
    }
    public boolean delete(int id){
        return bookingDaoFile.delete(id);
    }

    public int getMaxId(){
        return bookingDaoFile.getAll()
                .stream()
                .mapToInt(Booking::id)
                .max()
                .orElse(0);
    }
}
