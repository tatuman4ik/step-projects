package entity;

import dao.Identifiable;
import java.io.Serializable;
import java.time.*;
import java.time.format.*;
import java.util.Objects;

public class Flight implements Serializable, Identifiable {
    private final int id;
    private final Airline airline;
    private final City departure;
    private final City destination;
    private final LocalDateTime flightDateTime;
    private final int capacity;
    private Board board;

    public Flight(Airline airline, City departure, City destination, LocalDateTime flightDateTime, int capacity, int id) {
        this.airline = airline;
        this.departure = departure;
        this.destination = destination;
        this.flightDateTime = flightDateTime;
        this.board = new Board(capacity);
        this.capacity = board.getCapacity();
        this.id = id;
    }

    public String prettyFormat() {
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH:mm");
        return String.format("%4d %4s %13s - %-13s %12s %6s",
                id(), getAirline().getCode(), getDeparture().getName(), getDestination().getName(),
                getFlightDateTime().format(formatter1), getFlightDateTime().format(formatter2));
    }

    public Airline getAirline() {
        return airline;
    }

    public City getDeparture() {
        return departure;
    }

    public City getDestination() {
        return destination;
    }

    public LocalDateTime getFlightDateTime() {
        return flightDateTime;
    }

    public int getCapacity() { return capacity; }

    public Board getBoard() {
        return board;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return id == flight.id &&
                capacity == flight.capacity &&
                airline == flight.airline &&
                Objects.equals(departure, flight.departure) &&
                Objects.equals(destination, flight.destination) &&
                Objects.equals(flightDateTime, flight.flightDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, airline, departure, destination, flightDateTime, capacity) * 31;
    }
}
