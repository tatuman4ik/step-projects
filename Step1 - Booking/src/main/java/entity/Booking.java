package entity;

import dao.Identifiable;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public record Booking(int userId, List<Ticket> tickets, int id) implements Serializable, Identifiable {

    public String prettyFormat() {
        return String.format("""
                        Booking № %4d
                            tickets:
                                    %s
                        """,
                id(), getTicketsPrettyFormat());
    }

    public String getTicketsPrettyFormat() {
        return tickets.stream()
                .map(Ticket::getPrettyFormat)
                .collect(Collectors.joining("\n\t\t\t"));
    }
}
