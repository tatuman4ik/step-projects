package entity;

import dao.Identifiable;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;

public class Ticket implements Serializable, Identifiable {
    private final String passenger;
    private final Flight flight;
    private final Place place;
    private final int id;
    private final String pnrCode;

    public Ticket(String passenger, Flight flight, Place place) {
        this.passenger = passenger;
        this.flight = flight;
        this.place = place;
        this.pnrCode = TicketKeyGenerator();
        this.id = pnrCode.hashCode();
    }

    public String TicketKeyGenerator() {
        StringBuilder sb = new StringBuilder();
        return sb.append(flight.getAirline().getCode())
                .append(flight.getDeparture().getName())
                .append(flight.getDestination().getName())
                .append(flight.getFlightDateTime().format(DateTimeFormatter.ofPattern("ddMMyyHHmm")))
                .append(passenger)
                .append(place.toString())
                .toString()
                .toUpperCase();
    }

    @Override
    public int id() {
        return id;
    }

    public String getPassenger() {
        return passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public Place getPlace() {
        return place;
    }

    public int getId() {
        return id;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public String getPrettyFormat(){
        return String.format("Name:%s %s Place:%s",
                passenger,
                flight.prettyFormat(),
                place.toString());
    }
}
