package entity;

public enum Seat {
    A("Window Seat", 0),
    B("Middle Seat", 1),
    C("Aisle Seat", 2),
    D("Aisle Seat", 3),
    E("Middle Seat", 4),
    F("Window Seat", 5);

    private final String seatName;
    private final int index;

    Seat(String seatName, int index) {
        this.seatName = seatName;
        this.index = index;
    }

    public String getSeatName() {
        return this.seatName;
    }

    public int getIndex() {
        return this.index;
    }
}
