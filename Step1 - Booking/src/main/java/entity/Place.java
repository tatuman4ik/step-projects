package entity;

import java.io.Serializable;
import java.util.Objects;

public class Place implements Serializable {
    public int row;
    public Seat seat;
    public boolean vacant = true;

    public Place(int row, Seat seat) {
        this.row = row;
        this.seat = seat;
    }

    public int getRow() {
        return row;
    }

    public Seat getSeat() {
        return seat;
    }

    public boolean isVacant() {
        return vacant;
    }

    public boolean bookPlace() {
        if (vacant) {
            vacant = false;
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%d%s", row, seat);
    }

    public boolean setVacant() {
        if (!vacant) {
            vacant = true;
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return row == place.row &&
                seat == place.seat &&
                vacant == place.vacant;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, seat, vacant);
    }
}
