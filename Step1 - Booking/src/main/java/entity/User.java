package entity;

import dao.Identifiable;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable, Identifiable{
    private final int id;
    private final String login;
    private final String password;

    public User(String login, String password, int id) {
        this.login = login;
        this.password = password;
        this.id = id;
    }

    public User() {
        login = "guest";
        password = "guest";
        id = 0;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
